import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompraDeVuelosComponent } from './compra-de-vuelos.component';

describe('CompraDeVuelosComponent', () => {
  let component: CompraDeVuelosComponent;
  let fixture: ComponentFixture<CompraDeVuelosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompraDeVuelosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompraDeVuelosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
