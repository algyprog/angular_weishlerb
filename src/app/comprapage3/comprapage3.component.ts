import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-comprapage3',
  templateUrl: './comprapage3.component.html',
  styleUrls: ['./comprapage3.component.css']
})
export class Comprapage3Component implements OnInit {

  pageactual(){
    window.alert('Ya estas en esta pagina.')
  }

  alert(){
    window.alert('Tu boleto para el vuelo mas proximo se ha añadido al carro.')
  }
  alert2(){
    window.alert('Tu boleto para el vuelo con esta fecha se ha añadido al carro.')
  }

  precio = "70$ USD";

  constructor() { }

  ngOnInit(): void {
  }

}
