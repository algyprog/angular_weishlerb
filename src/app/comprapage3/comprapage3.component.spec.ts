import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Comprapage3Component } from './comprapage3.component';

describe('Comprapage3Component', () => {
  let component: Comprapage3Component;
  let fixture: ComponentFixture<Comprapage3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Comprapage3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Comprapage3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
