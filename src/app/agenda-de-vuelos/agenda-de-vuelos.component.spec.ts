import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgendaDeVuelosComponent } from './agenda-de-vuelos.component';

describe('AgendaDeVuelosComponent', () => {
  let component: AgendaDeVuelosComponent;
  let fixture: ComponentFixture<AgendaDeVuelosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgendaDeVuelosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgendaDeVuelosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
