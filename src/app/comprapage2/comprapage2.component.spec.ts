import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Comprapage2Component } from './comprapage2.component';

describe('Comprapage2Component', () => {
  let component: Comprapage2Component;
  let fixture: ComponentFixture<Comprapage2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Comprapage2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Comprapage2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
