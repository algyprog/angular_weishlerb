import { NgModule } from '@angular/core';
import { Routes,RouterModule } from '@angular/router';
import { AgendaDeVuelosComponent } from '../agenda-de-vuelos/agenda-de-vuelos.component';
import { CompraDeVuelosComponent } from '../compra-de-vuelos/compra-de-vuelos.component';
import { UbicacionDeAgenciasComponent } from '../ubicacion-de-agencias/ubicacion-de-agencias.component';
import { ContactanosComponent } from '../contactanos/contactanos.component';
import { SobreNosotrosComponent } from '../sobre-nosotros/sobre-nosotros.component';
import { HomeComponent } from '../home/home.component';
import { Homepage2Component } from '../homepage2/homepage2.component';
import { Homepage3Component } from '../homepage3/homepage3.component';
import { Comprapage2Component } from '../comprapage2/comprapage2.component';
import { Comprapage3Component } from '../comprapage3/comprapage3.component';
import { BoletosComponent } from '../boletos/boletos.component';
import { MainComponent } from '../main/main.component';


const routes: Routes = [
  { path: 'inicio', component: HomeComponent },
  { path: 'news', component: MainComponent },
  { path: 'homepage2', component: Homepage2Component },
  { path: 'homepage3', component: Homepage3Component },
  { path: 'agenda-de-vuelos', component: AgendaDeVuelosComponent },
  { path: 'compra-de-vuelos', component: CompraDeVuelosComponent },
  { path: 'ubicacion_de_agencias', component: UbicacionDeAgenciasComponent },
  { path: 'contactanos', component: ContactanosComponent },
  { path: 'sobre_nosotros', component: SobreNosotrosComponent },
  { path: 'Comprapage2', component: Comprapage2Component  },
  { path: 'Comprapage3', component: Comprapage3Component  },
  { path: 'Boletos', component: BoletosComponent },
  { path: '**', component: HomeComponent }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes),
  ],
  exports:[
    RouterModule
  ]
})
export class AppRoutingModule { }
