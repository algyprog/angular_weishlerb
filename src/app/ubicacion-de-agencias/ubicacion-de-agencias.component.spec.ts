import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UbicacionDeAgenciasComponent } from './ubicacion-de-agencias.component';

describe('UbicacionDeAgenciasComponent', () => {
  let component: UbicacionDeAgenciasComponent;
  let fixture: ComponentFixture<UbicacionDeAgenciasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UbicacionDeAgenciasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UbicacionDeAgenciasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
