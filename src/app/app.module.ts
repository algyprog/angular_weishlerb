import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing/app-routing.module';
import { AppComponent } from './app.component';
import { AirlinesComponent } from './airlines/airlines.component';
import { MainComponent } from './main/main.component';
import { CardsComponent } from './cards/cards.component';
import { FooterComponent } from './footer/footer.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { AgendaDeVuelosComponent } from './agenda-de-vuelos/agenda-de-vuelos.component';
import { CompraDeVuelosComponent } from './compra-de-vuelos/compra-de-vuelos.component';
import { UbicacionDeAgenciasComponent } from './ubicacion-de-agencias/ubicacion-de-agencias.component';
import { ContactanosComponent } from './contactanos/contactanos.component';
import { SobreNosotrosComponent } from './sobre-nosotros/sobre-nosotros.component';
import { HomeComponent } from './home/home.component';
import { Homepage2Component } from './homepage2/homepage2.component';
import { Homepage3Component } from './homepage3/homepage3.component';
import { Comprapage2Component } from './comprapage2/comprapage2.component';
import { Comprapage3Component } from './comprapage3/comprapage3.component';
import { BoletosComponent } from './boletos/boletos.component';
import { CartComponent } from './cart/cart.component';


@NgModule({
  declarations: [
    AppComponent,
    AirlinesComponent,
    MainComponent,
    CardsComponent,
    FooterComponent,
    ToolbarComponent,
    AgendaDeVuelosComponent,
    CompraDeVuelosComponent,
    UbicacionDeAgenciasComponent,
    ContactanosComponent,
    SobreNosotrosComponent,
    HomeComponent,
    Homepage2Component,
    Homepage3Component,
    Comprapage2Component,
    Comprapage3Component,
    BoletosComponent,
    CartComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
